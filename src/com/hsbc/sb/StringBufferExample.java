package com.hsbc.sb;

public class StringBufferExample 
{
    public static void main(String[] args)
    {
    	StringBuffer sb= new StringBuffer("InitialValue");
    	System.out.println(sb);
    	sb.append("+additionalvalue");
    	System.out.println(sb);
    	sb.replace(0,5,"abcde");
    	System.out.println(sb);
    	
    	//explore 7 to 8 methods.
    	//get diff between String buffer & string buffer
    }
}
