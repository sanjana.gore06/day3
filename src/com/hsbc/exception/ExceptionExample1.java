package com.hsbc.exception;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionExample1 {

	public static void main(String[] args) 
	{
		System.out.println("Please enter 2 value");
		Scanner scan = new Scanner(System.in);
		
		try
		{
		
		int a = scan.nextInt();
		int b = scan.nextInt();
		int div = a/b;
		System.out.println("Division is: "+div);
		}
		catch(ArithmeticException ae)
		{
			System.err.println("Exception occurred !!! Division not possible as second number is 0");
		     System.out.println(ae.getMessage());
		}
		catch(InputMismatchException ae)
		{
			System.out.println("Handle Exception!!!");
		}
		// TODO Auto-generated method stub
		
		System.out.println("TADDAA");

	}

}
