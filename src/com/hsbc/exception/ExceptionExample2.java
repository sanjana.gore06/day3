package com.hsbc.exception;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionExample2 {

	public static void main(String[] args)
	{
		System.out.println("Please enter some string");
		Scanner scan = new Scanner(System.in);
		String str= scan.next();
		System.out.println("Please enter position");
		int pos = scan.nextInt();
		try
		{
			System.out.println(str.charAt(pos));	
		}
		catch(StringIndexOutOfBoundsException se)
		{
			System.out.println("EXception Occured!!");
		}
		catch(InputMismatchException ee)
		{
			System.out.println("Handle Exception!!!");
		}
		
		// TODO Auto-generated method stub

	}

}
